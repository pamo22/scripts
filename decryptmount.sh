cryptsetup open $1 dencrypt
mount /dev/mapper/dencrypt /mnt/enc --mkdir
cd /mnt/enc
echo 'now entering a bash terminal.'
bash
cd /
umount -f /mnt/enc 
cryptsetup close dencrypt
echo "" > /root/.bash_history
